$(function() {
    $(".slides-js").slidesjs({
        width: 960,
        height: 530,
        start: 1,
        play: {
            auto: true,
            interval: 5000,
            pauseOnHover: true
        },
        navigation: {
            active: false
        },
        pagination: {
            active: false
        }
    });
});
